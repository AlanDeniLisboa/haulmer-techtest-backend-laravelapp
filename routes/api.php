<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\TokenAuthMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//User Controller Utilizes an Internal Guard
Route::get('/me', [UserController::class, 'getMyProfile']);
Route::put('/me', [UserController::class, 'updateMyProfile']);
Route::post('/me', [UserController::class, 'updateMyProfile']);
Route::delete('/me', [UserController::class, 'deleteMyProfile']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/new', [AuthController::class, 'signup']);
