<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

use function PHPUnit\Framework\isEmpty;

class UsersService {
    public function Save(User $user){
        $response = Http::post(Config::get('api.users'), [
            'id' => $user->id,
            'email' => $user->email,
            'password' => $user->password,
            'name' => $user->name
        ]);

        if($response->failed()){
            return false; //TODO: Throw error, Save Request Failed
        } else {
            return true;
        }
    }

    public function Update(User $user){
        $res = Http::put(Config::get('api.users').'/'.$user->id, [
            'id' => $user->id,
            'email' => $user->email,
            'password' => $user->password,
            'name' => $user->name
        ]);
        if($res->failed()){
            return false; //TODO: THROW ERROR
        } else {
            return true;
        }

    }

    public function Get($user_id){
        $response = http::get(config::get('api.users'), [
            'id' => $user_id
        ]);
        if($response->failed()){
            return false; //TODO: Throw error, Save Request Failed
        }
        if(empty($response->json())){
            return false; //TODO: Throw User doesn't exist
        }
        else {
            $user_json = $response->json()[0];
            $user = new User;
            $user->id = $user_id;
            $user->email = $user_json['email'];
            $user->password = $user_json['password'];
            $user->name = $user_json['name'];
            return $user;
        }
    }

    public function EmailDoesNotExist($email){
        $response = http::get(config::get('api.users'), [
            'email' => $email
        ]);
        if($response->failed()){
            return false; //TODO: Throw error, Save Request Failed
        }
        if(empty($response->json())){
            return true;
        }
        else {
            return false; //TODO: Log User Exists
        }
    }

    public function DeleteUser($user_id) {
        $response = http::delete(config::get('api.users').'/'.$user_id, []);
        if($response->failed()){
            return false; //TODO: Throw error, Save Request Failed
        }
        else {
            return true;
        }
    }
}
