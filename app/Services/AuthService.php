<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Services\TokenService;
use App\Services\UsersService;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class AuthService {
    public function __construct(TokenService $tokenService, UsersService $usersService)
    {
        $this->usersService = $usersService;
        $this->tokenService = $tokenService;
    }

    public function login($email, $password){
        $response = Http::get(Config::get('api.users'), [
            'email' => $email,
            'password' => $password,
        ]);
        if($response->failed()){
            return false; //TODO: Log error type
        }
        if(empty($response->json()) OR !Hash::check($password, $response->json()[0]['password'])){
            return false; //TODO: Credentials are wrong
        }
        $user_id = $response->json()[0]['id'];
        $token = $this->tokenService->GenToken($user_id);
        return $token;
    }

    public function signUp($email, $password){
        if($this->usersService->EmailDoesNotExist($email)){
            $user = new User;
            $user->fill([
                'email' => $email,
                'password' => Hash::make($password)
            ]);
            if($this->usersService->Save($user)){
                return true;
            } else {
                return false; //TODO: Throw Error, User wasn't save into API
            }
        }
    }

    public function LoggedInGuard(Request $request, Closure $next)
    {
        if(empty($request->bearerToken())){
            return response()->json([
                'message' => 'Unauthorized',//TODO: Localize this
                'error' => 401
            ], 401);
        } else {
            $user_id = $this->tokenService->GetUserId($request->bearerToken());
            if($user_id){
                $user = $this->usersService->Get($user_id);
                if($user){
                    $request->user = $user;
                } else {
                    return response()->json([
                        'message' => 'Unauthorized',//TODO: Localize this
                        'error' => 401
                    ], 401);
                }
            }else {
                return response()->json([
                    'message' => 'Unauthorized',//TODO: Localize this
                    'error' => 401
                ], 401);
            }
        }
        return $next($request);
    }
}
