<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use function PHPUnit\Framework\isEmpty;

class TokenService {

    /*
     * @param user_id
     * @return tokenstring
     *  */
    public function GenToken($user_id){
        $token = Str::random(64);
        if($this->SaveToken($user_id, $token)){
            return $token;
        } else {
            return false; //TODO: Log Error
        }
    }

    protected function SaveToken($user_id, $token){
        $response = Http::post(Config::get('api.tokens'), [
            'userId' => $user_id,
            'token' => $token
        ]);

        if($response->failed()){
            return false; // TODO: Throw Exception, Token wasn't save into API
        }
        else {
            return true;
        }
    }

    public function GetUserId($token){
        $response = Http::get(Config::get('api.tokens'), ['token' => $token]);
        if($response->failed()){
            return false; //TODO Throw Exception, Can't get a get from the API
        }
        if(empty($response->json())) {
            return false; //TODO Log Token doesn't exist
        }
        $token = $response->json()[0];
        return $token['userId'];
    }
}
