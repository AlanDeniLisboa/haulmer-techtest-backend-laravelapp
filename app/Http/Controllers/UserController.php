<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use App\Services\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct(AuthService $authService, UsersService $usersService)
    {
        $this->usersService = $usersService;
        $this->authService = $authService;
    }

    public function getMyProfile(Request $request){
        return $this->authService->LoggedInGuard($request, function($request){
            return response()->json([
                'name' => $request->user->name,
                'email' => $request->user->email,
                'password' => $request->user->password
            ],200);
        });
    }

    public function updateMyProfile(Request $request){
        return $this->authService->LoggedInGuard($request, function(Request $request){
            $user = $request->user;
            if(!empty($request->get('email')) AND !($request->get('email') === '')){
                $user->email = $request->get('email');
            }
            if(!empty($request->get('password'))){
                $user->password = Hash::make($request->get('password'));
            }
            if(!empty($request->get('name'))){
                $user->name = $request->get('name');
            }
            if($this->usersService->Update($user)){
                return response()->json([], 200);
            } else {
                return response()->json([
                    'error' => 500,
                    'message'=> 'An error has happen while saving the user', //TODO: LOCALIZE THIS
                ], 500);
            }
        });
    }

    public function deleteMyProfile(Request $req){
        return $this->authService->LoggedInGuard($req, function(Request $req){
            $user = $req->user;
            $res = $this->usersService->DeleteUser($user->id);
            if($res){
                return response()->json([], 200);
            } else {
                return response()->json([
                    'code'=> 500,
                    'message'=> "Error deleting current user"
                ], 500);
            }
        });
    }



}
