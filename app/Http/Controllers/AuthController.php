<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function __construct(AuthService $authService)
    {
        $this->authService= $authService;
    }

    public function login(Request $request){

        $rules = [
            "email" => "required|email",
            "password" => "required",
        ];

        $validated = $request->validate($rules);
        $res = $this->authService->login($validated['email'], $validated['password']);
        if($res){
            return response()->json([
                'token' => $res,
            ], 200);
        } else {
            //TODO: Error messages for server and API error
            return response()->json([
                'code' => 401,
                'message' => 'Invalid Credentials'
            ], 401);
        }
    }

    public function signup(Request $request){
        $rules = [
            "email" => "required|email",
            "password" => "required",
        ];

        $validated = $request->validate($rules);

        $res = $this->authService->signUp($validated['email'], $validated['password']);
        if($res){
            return response()->json([], 200);
        } else {
            //TODO: Server and API Failure Error Message
            return response()->json([
                'code' => 401,
                'message' => 'Credentials are invalid',
            ], 401);
        }
    }
}

