<?php
return [
    'users' => env('MOCKAPI_USERS_ENDPOINT', ''),
    'tokens' => env('MOCKAPI_TOKENS_ENDPOINT', ''),
];
